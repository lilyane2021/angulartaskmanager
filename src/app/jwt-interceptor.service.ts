import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JwtInterceptorService  implements HttpInterceptor{

  constructor() { }
  
  
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    var currentUser = {token:""};
    if(sessionStorage['currentUser'] !=null){
      currentUser=JSON.parse(sessionStorage['currentUser']);
    }
  
    
    req = req.clone({
      setHeaders: {
        'Content-Type' : 'application/json; charset=utf-8',
        'Accept'       : 'application/json',
        'Authorization': `Bearer ${currentUser.token}`,
        'Access-Control-Allow-Origin':"*",
        'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
        'Access-Control-Allow-Headers': "Accept,authorization,Authorization, Content-Type"
      },
    });
    
    return next.handle(req);
    
  }
  
  
}
