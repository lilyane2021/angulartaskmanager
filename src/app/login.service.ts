import { HttpBackend, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { map, Observable } from 'rxjs';
import { LoginViewModel } from './login-view-model';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private httpClient: HttpClient|null=null;
  
  constructor(private httpBackend :HttpBackend, private jwtHelperService:JwtHelperService) { 
    
  }
  urlPrefix: string = "http://localhost:54573";
  currentUserName:any=null;
  
  public Login(loginViewModel : LoginViewModel):Observable<any>
  {
    this.httpClient= new HttpClient(this.httpBackend)
    return this.httpClient.post<any>("http://localhost:54573/authenticate",loginViewModel,{responseType:"json"})
    .pipe(map(user=>{
      if(user)
      {
        console.log(user);
        this.currentUserName=user.userName;
        sessionStorage.setItem('currentUser',JSON.stringify(user));
        console.log(sessionStorage);
      }
      return user;
    }));
  }
  
  public LogOut()
  {
    sessionStorage.removeItem("currentUser");
    this.currentUserName=null;
  }
  
  public isAuthenticated():boolean
  {
    var token = sessionStorage.getItem("currentUser")?JSON.parse(sessionStorage.getItem("currentUser")as string).token:null;
    if(this.jwtHelperService.isTokenExpired()){
      return false;//token is not valid
    }
    else{
      return true; //token is valid
    }
  }
}
