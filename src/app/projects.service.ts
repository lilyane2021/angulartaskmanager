import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import { Observable } from 'rxjs';
import { Project } from './project';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {

  
  urlPrefix: string = "http://localhost:54573";
  constructor(private httpClient:HttpClient) { }
  
  
  getAllProjects():Observable<Project[]>
  {
    // var currentUser={token:""};
    // var headers= new HttpHeaders();
    
    // headers =headers.set("Authorization","Bearer");
    
    // if(sessionStorage['currentUser'] !=null){
    //   currentUser=JSON.parse(sessionStorage['currentUser']);
      
    //   console.log("currentUser is : ");
    //   console.log(currentUser.token);
    //   headers= headers.set("Authorization","Bearer"+currentUser.token);
    // }
    // console.log("the headers are : ");
    // console.log(headers);
    return this.httpClient.get<Project[]>("http://localhost:54573/api/Projects",{responseType:"json"});
  }
  
  
    
  
  insertProject(newProject:Project):Observable<Project>
  {
    const headers = {'Content-Type': 'application/json'};
    const body=JSON.stringify(newProject);
    console.log(body);
    return this.httpClient.post<Project>("http://localhost:54573/api/Projects",body,{'headers':headers,responseType:"json"});
  }
  
  updateProject(existingProject:Project):Observable<Project>
  {
     return this.httpClient.put<Project>("http://localhost:54573/api/Projects",existingProject,{responseType:"json"});
  }
  deleteProject(ProjectID: number): Observable<string>
  {
    return this.httpClient.delete<string>(this.urlPrefix + "/api/projects?ProjectID=" + ProjectID);
  }
  
  SearchProjects(searchBy: string, searchText: string): Observable<Project[]>
  {
    return this.httpClient.get<Project[]>(
      this.urlPrefix + '/api/projects/search/' + searchBy + '/' + searchText,
      { responseType: 'json' }
    );
  }
  
}
